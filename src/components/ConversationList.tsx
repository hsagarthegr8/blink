import React, { Component } from 'react'

import { List, Theme, createStyles, withStyles, WithStyles } from '@material-ui/core';
import ConversationListItem from './ConversationListItem'
import { ConversationList as Conversations, ConversationBasic } from '../store/chats';
import { ApplicationState } from '../store';
import { connect } from 'react-redux';
import Scrollbar from 'react-scrollbars-custom';

interface StoreProps {
    conversations: Conversations,
}

interface OwnProps {
    query: string
}

const styles = (theme: Theme) => createStyles({
    root: {
        width: '100%',
        maxWidth: 360,
        backgroundColor: theme.palette.background.paper,
    },
})

class ConversationList extends Component<OwnProps & StoreProps & WithStyles<typeof styles>> {
    render() {
        const { classes, conversations, query } = this.props
        const filtered_conversations = !query ? conversations : conversations.filter(
            (conversation) => conversation.recipient.fullName.toLocaleLowerCase()
                .includes(query.toLocaleLowerCase())
        )

        return (
            <Scrollbar style={{ height: '80%' }}>
                <List className={classes.root}>
                    {filtered_conversations.length ?
                        filtered_conversations.map((conversation: ConversationBasic) =>
                            <ConversationListItem
                                key={conversation.id}
                                conversation={conversation}
                            />
                        )
                        :
                        null
                    }
                </List>
            </Scrollbar>
        )
    }
}

const mapStateToProps = (state: ApplicationState): StoreProps => {
    return {
        conversations: state.chats.conversationList
    }
}

export default connect(mapStateToProps)(withStyles(styles)(ConversationList))