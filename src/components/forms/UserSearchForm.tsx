import React, { FC } from 'react'
import { TextField, InputAdornment, IconButton } from '@material-ui/core'
import { Magnify, Close } from 'mdi-material-ui'


interface OwnProps {
    query: string,
    handleChange: Function
}

const UserSearchForm: FC<OwnProps> = props => {
    const { query, handleChange } = props
    return (
        <TextField
            name="query"
            placeholder="Search User"
            variant="outlined"
            value={query}
            onChange={(e) => handleChange(e.target.value)}
            fullWidth
            InputProps={{
                autoComplete: 'off',
                startAdornment: (
                    <InputAdornment position="start">
                        <Magnify />
                    </InputAdornment>
                ),
                endAdornment: ( query && (
                        <InputAdornment position="end">
                            <IconButton onClick={(e) => handleChange('')}>
                                <Close />
                            </IconButton>
                        </InputAdornment>
                    )
                )
            }}
        />
    )
}

export default UserSearchForm