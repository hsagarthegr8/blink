import React, { FC } from 'react'
import { withFormik, FormikBag, FormikProps } from 'formik'
import * as Yup from 'yup'
import { ApplicationState } from '../../store';
import { connect } from 'react-redux';
import { Theme, createStyles, WithStyles, withStyles, TextField, 
    Fab, Grid } from '@material-ui/core';
import { Send } from 'mdi-material-ui';
import { sendMessageRequest } from '../../store/websocket/action';


interface StoreProps {
    username?: string,
    selectedConversation: string
}

interface Action {
    sendMessageRequest: Function
}

interface Values {
    content: string
}

const formConfig = withFormik<StoreProps & Action, Values>({
    mapPropsToValues: (): Values => ({
        content: ''
    }),

    validationSchema: Yup.object().shape<Values>({
        content: Yup.string().required()
    }),

    handleSubmit: (values: Values, formikBag: FormikBag<StoreProps & Action, Values>) => {
        const { sendMessageRequest, selectedConversation } = formikBag.props
        sendMessageRequest(selectedConversation, values.content)
        formikBag.resetForm()
    }
})

const styles = (theme: Theme) => createStyles({

})

const MessageForm: FC<StoreProps & Action & FormikProps<Values> 
                        & WithStyles<typeof styles>> = props => {
    const { values, handleChange, isValid, handleSubmit } = props
    return (
        <form>
            <Grid container spacing={4}>
                <Grid item xs={12} sm container>
                    <Grid item xs>
                        <TextField
                            placeholder="Enter your message"
                            name="content"
                            value={values.content}
                            onChange={handleChange}
                            variant="outlined"
                            fullWidth
                            multiline
                            rows="1"
                        />
                    </Grid>
                </Grid>
                <Grid item>
                    <Fab color='primary' disabled={!isValid} onClick={()=>handleSubmit()}>
                        <Send />
                    </Fab>                
                </Grid>
            </Grid>
        </form>
    )
}

const mapStateToProps = (state: ApplicationState): StoreProps => {
    return {
        username: state.auth.username,
        selectedConversation: state.chats.selectedConversation
    }
}

const mapDispatchToProps: Action = {
    sendMessageRequest
}

export default connect(mapStateToProps, mapDispatchToProps)(formConfig(withStyles(styles)(MessageForm)))