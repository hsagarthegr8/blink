import React, { FC } from 'react'
import { TextField, InputAdornment, IconButton } from '@material-ui/core'
import { Magnify, Close } from 'mdi-material-ui';

interface OwnProps {
    query: string,
    handleChange: Function
}

const ConversationSearchForm: FC<OwnProps> = props => {
    const { query, handleChange } = props
    return (
        <div style={{padding:'0.5em'}}>
            <TextField 
                name="query"
                placeholder="Search Conversation"
                variant="outlined"
                value={query}
                onChange={(e)=>handleChange(e)}
                fullWidth
                InputProps={{
                    autoComplete: 'off',
                    startAdornment: (
                        <InputAdornment position="start">
                            <Magnify />
                        </InputAdornment>
                    ),
                    endAdornment: ( query && (
                            <InputAdornment position="end">
                                <IconButton onClick={(e) => handleChange(e, true)}>
                                    <Close />
                                </IconButton>
                            </InputAdornment>
                        )
                    )
                }}
            />
        </div>
    )
}

export default ConversationSearchForm