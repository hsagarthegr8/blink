import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withFormik, FormikProps, FormikBag } from 'formik'
import * as Yup from 'yup'
import { AxiosResponse, AxiosError } from 'axios'
import {
    Button,
    TextField,
    WithStyles,
    withStyles,
    createStyles,
    Theme,
    InputAdornment,
    IconButton,
    Typography,
    Paper
} from '@material-ui/core'
import { Eye, EyeOff, AccountOutline, KeyOutline } from 'mdi-material-ui'

import Api, { loginUrl } from '../../api'
import { logIn } from '../../store/auth/action'
import { requiredErrorMessage } from '../../utils';
import { RouteComponentProps, withRouter } from 'react-router';


interface Values extends NonFieldErrors { 
    username: string,
    password: string
}

interface NonFieldErrors {
    non_field_errors?: string
}

interface Action {
    logIn: Function
}

interface State {
    showPassword: boolean
}

const formContainer = withFormik<RouteComponentProps & Action, Values>({
    mapPropsToValues: (): Values => ({
        username: '',
        password: '',
    }),

    validationSchema: Yup.object().shape<Values>({
        username: Yup.string().required(requiredErrorMessage),
        password: Yup.string().required(requiredErrorMessage)
    }),

    handleSubmit: (values: Values, formikBag: FormikBag<RouteComponentProps & Action, Values>) => {
        const api = Api()
        const { logIn } = formikBag.props
        api.post(loginUrl, values)
        .then((res: AxiosResponse) => {
            const { token } = res.data
            logIn(token)
            const { history } = formikBag.props
            history.push('/chats/')
        })
        .catch((err: AxiosError) => {
            if (err.response)
                formikBag.setFieldValue('non_field_errors', err.response.data.non_field_errors[0])
        })
    },
})


const styles = (theme:Theme) => createStyles({
    textField: {
        marginTop:'1em'
    },
    buttonGroup: {
        float: "right",
        marginTop: '1em'
    },
    formButton: {
        marginLeft: '1em'
    }
})

class LoginForm extends Component<Action & FormikProps<Values> & RouteComponentProps
                            & WithStyles<typeof styles>, State> {

    state = {
        showPassword: false
    }

    handleToggleShowPassword = () => {
        this.setState(prevState => ({
            showPassword: !prevState.showPassword
        }))
    }

    handleChange = (event:any) => {
        const {
            handleChange,
            setFieldTouched
        } = this.props
        handleChange(event)
        setFieldTouched(event.target.name)
    }

    render() {
        const { errors, touched, values, handleSubmit, resetForm, classes } = this.props
        return (
            <Paper style={{maxWidth:'30em', padding:'1em', margin:'auto'}}>
                <Typography variant="h5">Login Now</Typography>
                <form>
                    <Typography color="error">{values.non_field_errors}</Typography>
                    <TextField
                        name="username"
                        variant="outlined"
                        value={values.username}
                        label="Username"
                        className={classes.textField}
                        InputProps={{
                            startAdornment: (
                                <InputAdornment position="start"><AccountOutline color="primary"/></InputAdornment>
                            )
                        }}
                        helperText={touched.username ? errors.username : ""}
                        error={(touched.username && Boolean(errors.username)) || Boolean(values.non_field_errors)}
                        fullWidth
                        required
                        onChange={this.handleChange}
                    />
                    <TextField
                        name="password"
                        label="Passwod"
                        variant="outlined"
                        className={classes.textField}
                        helperText={touched.password ? errors.password : ""}
                        error={(touched.password && Boolean(errors.password)) || Boolean(values.non_field_errors)}
                        value={values.password}
                        InputProps={{
                            startAdornment: (
                                <InputAdornment position="start">
                                    <KeyOutline color="primary" />
                                </InputAdornment>
                            ),
                            endAdornment: (
                                <InputAdornment position="end">
                                    <IconButton onClick={this.handleToggleShowPassword}>
                                        {this.state.showPassword ? <EyeOff /> : <Eye />}
                                    </IconButton>
                                </InputAdornment>
                            )
                        }}
                        fullWidth
                        required
                        type={this.state.showPassword ? 'text' : 'password'}
                        onChange={this.handleChange}
                    />

                    <div className={classes.buttonGroup}>
                        <Button 
                            className={classes.formButton} 
                            variant="contained"
                            color="primary"
                            onClick={()=>handleSubmit()}
                        >
                            Log In
                        </Button>

                        <Button 
                            className={classes.formButton} 
                            variant="contained" 
                            color="primary" 
                            onClick={()=>resetForm()}
                        >
                        Reset
                        </Button>
                    </div>
                </form>
            </Paper>
        )
    }
}


const mapDispatchToProps = {
    logIn,
}

export default connect(null, mapDispatchToProps)(
    withRouter(formContainer(withStyles(styles)(LoginForm)))
)