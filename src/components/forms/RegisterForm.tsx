import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withFormik, FormikProps, FormikBag } from 'formik'
import * as Yup from 'yup'
import { createStyles, Theme, WithStyles, withStyles, Paper, 
    Button, Typography, TextField, InputAdornment, IconButton } from '@material-ui/core';
import { Eye, EyeOff, AccountOutline, EmailOutline, KeyOutline } from 'mdi-material-ui'
import Api, { registerUrl } from '../../api';
import { AxiosResponse, AxiosError } from 'axios';
import { withRouter, RouteComponentProps } from 'react-router';
import { logIn } from '../../store/auth'

interface Actions {
    logIn: Function
}

interface Values {
    username: string,
    email: string,
    password: string,
    confirmPassword: string
}

const formContainer = withFormik<Actions & RouteComponentProps, Values>({
    mapPropsToValues: () => ({
        username: '',
        email: '',
        password: '',
        confirmPassword: '',
    }),

    validationSchema: Yup.object().shape<Values>({
        username: Yup.string().required(),
        email: Yup.string().email().required(),
        password: Yup.string().required(),
        confirmPassword: Yup.string().required()
            .oneOf([Yup.ref('password')], "Password does not match")
    }),

    handleSubmit: (values: Values, formikBag: FormikBag<Actions & RouteComponentProps, Values>) => {
        const { logIn } = formikBag.props
        const api = Api()
        const user = {
            username: values.username,
            email: values.email,
            password: values.password
        }
        api.post(registerUrl, user)
        .then((res: AxiosResponse) => {
            logIn(res.data.token)
            const { history } = formikBag.props
            history.push('/welcome/')
        })
        .catch((err: AxiosError)=>{
            const errors: {[field: string]: any} = { }
            if (err.response) {
                const resErr = err.response.data
                for (const key in resErr) {
                    errors[key] = resErr[key][0]
                }
            }
            formikBag.setErrors(errors)
        })
    }
})

const styles = (theme:Theme) => createStyles({
    textField: {
        marginTop:'1em'
    },
    buttonGroup: {
        float: "right",
        marginTop: '1em'
    },
    formButton: {
        marginLeft: '1em'
    }
})

interface State {
    showPassword: boolean,
    showConfirmPassword: boolean
}


class RegisterForm extends Component<Actions & FormikProps<Values> & RouteComponentProps 
                                    & WithStyles<typeof styles>, State> {
    state = {
        showPassword: false,
        showConfirmPassword: false,
    }

    handleToggleShowPassword = () => {
        this.setState((prevState: State)=>({
            showPassword: !prevState.showPassword
        }))
    }

    handleToggleShowConfirmPassword = () => {
        this.setState((prevState: State)=>({
            showConfirmPassword: !prevState.showConfirmPassword
        }))
    }

    handleChange = (event:any) => {
        const {
            handleChange,
            setFieldTouched
        } = this.props
        handleChange(event)
        setFieldTouched(event.target.name)
    }

    render() {
        const { errors, touched, values, handleSubmit, resetForm, classes } = this.props
        return (
            <Paper style={{maxWidth:'30em', padding:'1em', margin:'auto'}}>
                <Typography variant="h5">Register</Typography>
                <form>
                    <TextField
                        name="username"
                        variant="outlined"
                        value={values.username}
                        label="Username"
                        className={classes.textField}
                        InputProps={{
                            startAdornment: (
                                <InputAdornment position="start"><AccountOutline color="primary"/></InputAdornment>
                            )
                        }}
                        helperText={touched.username ? errors.username : ""}
                        error={(touched.username && Boolean(errors.username))}
                        fullWidth
                        required
                        onChange={this.handleChange}
                    />
                    <TextField
                        name="email"
                        variant="outlined"
                        value={values.email}
                        label="Email"
                        className={classes.textField}
                        InputProps={{
                            startAdornment: (
                                <InputAdornment position="start"><EmailOutline color="primary"/></InputAdornment>
                            )
                        }}
                        helperText={touched.email ? errors.email : ""}
                        error={(touched.email && Boolean(errors.email))}
                        fullWidth
                        required
                        onChange={this.handleChange}
                    />
                    <TextField
                        name="password"
                        label="Passwod"
                        variant="outlined"
                        className={classes.textField}
                        helperText={touched.password ? errors.password : ""}
                        error={(touched.password && Boolean(errors.password))}
                        value={values.password}
                        InputProps={{
                            startAdornment: (
                                <InputAdornment position="start">
                                    <KeyOutline color="primary" />
                                </InputAdornment>
                            ),
                            endAdornment: (
                                <InputAdornment position="end">
                                    <IconButton onClick={this.handleToggleShowPassword}>
                                        {this.state.showPassword ? <EyeOff /> : <Eye />}
                                    </IconButton>
                                </InputAdornment>
                            )
                        }}
                        fullWidth
                        required
                        type={this.state.showPassword ? 'text' : 'password'}
                        onChange={this.handleChange}
                    />
                    <TextField
                        name="confirmPassword"
                        label="Confirm Password"
                        variant="outlined"
                        className={classes.textField}
                        helperText={touched.confirmPassword ? errors.confirmPassword : ""}
                        error={(touched.confirmPassword && Boolean(errors.confirmPassword))}
                        value={values.confirmPassword}
                        InputProps={{
                            startAdornment: (
                                <InputAdornment position="start">
                                    <KeyOutline color="primary" />
                                </InputAdornment>
                            ),
                            endAdornment: (
                                <InputAdornment position="end">
                                    <IconButton onClick={this.handleToggleShowConfirmPassword}>
                                        {this.state.showConfirmPassword ? <EyeOff /> : <Eye />}
                                    </IconButton>
                                </InputAdornment>
                            )
                        }}
                        fullWidth
                        required
                        type={this.state.showConfirmPassword ? 'text' : 'password'}
                        onChange={this.handleChange}
                    />

                    <div className={classes.buttonGroup}>
                        <Button 
                            className={classes.formButton} 
                            variant="contained"
                            color="primary"
                            onClick={()=>handleSubmit()}
                        >
                            Register
                        </Button>

                        <Button 
                            className={classes.formButton} 
                            variant="contained" 
                            color="primary" 
                            onClick={()=>resetForm()}
                        >
                        Reset
                        </Button>
                    </div>
                </form>
            </Paper>
        )
    }
}



export default connect(null, {logIn})(withRouter(formContainer(withStyles(styles)(RegisterForm))))
