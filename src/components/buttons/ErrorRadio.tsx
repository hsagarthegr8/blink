import React from 'react'
import { withStyles, Radio, } from '@material-ui/core'
import { red} from '@material-ui/core/colors'

const ErrorRadio = withStyles({
    root: {
      color: red[400],
      '&$checked': {
        color: red[600],
      },
    },
    checked: {},
})(props => <Radio color="default" {...props} />);
  
export default ErrorRadio