import React, { ComponentType, FC, MouseEventHandler } from 'react'

import { Button, Tooltip, Typography } from '@material-ui/core'
import { SvgIconProps } from '@material-ui/core/SvgIcon'

interface OwnProps {
    title: string,
    Icon: ComponentType<SvgIconProps>,
    onClick: MouseEventHandler<HTMLElement>
} 



const NavButton: FC<OwnProps> = (props) => {
    const { title, Icon, onClick } = props
    return (
        <Tooltip title={title}>
            <Button color="primary" onClick={onClick}>
                <Icon />
                &nbsp;
                <Typography display="inline" color="primary" variant="button">{title}</Typography>
            </Button>
        </Tooltip>
    )
}

export default NavButton