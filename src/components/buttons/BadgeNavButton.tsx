import React, { ComponentType, FC, MouseEventHandler } from 'react'

import { Button, Tooltip, Typography, Badge } from '@material-ui/core'
import { SvgIconProps } from '@material-ui/core/SvgIcon'

interface OwnProps {
    title: string,
    Icon: ComponentType<SvgIconProps>,
    onClick: MouseEventHandler<HTMLElement>,
    badge: number
} 



const BadgeNavButton: FC<OwnProps> = (props) => {
    const { title, Icon, onClick, badge } = props
    return (
        <Tooltip title={title}>
            <Button color="primary" onClick={onClick}>
                <Badge badgeContent={badge} color="error">
                    <Icon />
                </Badge>
                &nbsp;
                <Typography display="inline" color="primary" variant="button">{title}</Typography>
            </Button>
        </Tooltip>
    )
}

export default BadgeNavButton