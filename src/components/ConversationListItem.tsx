import React, { Component } from 'react'
import {
    ListItem, ListItemAvatar, Avatar,
    ListItemText, Typography, Theme, createStyles, withStyles, WithStyles, ListItemSecondaryAction, Badge
} from '@material-ui/core'
import { ConversationBasic, selectConversationAndGetMessages } from '../store/chats'
import { parseTime } from '../utils'

import avatar from '../images/user.png'
import { connect } from 'react-redux';
import { ApplicationState } from '../store';
import { CheckAll } from 'mdi-material-ui';

const styles = (theme: Theme) => createStyles({
    root: {
        cursor: 'default'
    },
    unread: {
        fontWeight: 'bold'
    },
    readReciepts: {
        position: 'relative',
        top: 4,
        marginLeft: 4
    }
})

interface StoreProps {
    selectedConversation: string,
    username?: string
}
interface OwnProps {
    conversation: ConversationBasic
}

interface Actions {
    selectConversationAndGetMessages: Function
}

class ConversationListItem extends Component<OwnProps & StoreProps & Actions
    & WithStyles<typeof styles>> {

    handleClick = () => {
        const { selectConversationAndGetMessages, conversation } = this.props
        selectConversationAndGetMessages(conversation.id)
    }

    render() {
        const { classes, conversation, selectedConversation, username } = this.props
        const selected = selectedConversation === conversation.id
        return (
            <ListItem
                alignItems="flex-start"
                onClick={this.handleClick}
                selected={selected}
                className={classes.root}
            >
                <ListItemAvatar>
                    <Badge color="primary" variant="dot" invisible={!conversation.unread}>
                        <Avatar alt={conversation.recipient.fullName} src={
                            conversation.recipient.image_url || avatar
                        } />
                    </Badge>
                </ListItemAvatar>
                <ListItemText
                    disableTypography={true}
                    secondary={
                        <Typography display="inline" >
                            {conversation.message.content}
                            {conversation.message.read && conversation.message.sender === username &&
                                <CheckAll color="primary" fontSize="small" className={classes.readReciepts} />
                            }
                        </Typography>
                    }
                >
                    <Typography>
                        {conversation.recipient.fullName}
                    </Typography>
                </ListItemText>
                <ListItemSecondaryAction>
                    <Typography variant="caption">
                        {parseTime(conversation.timestamp)}
                    </Typography>

                </ListItemSecondaryAction>
            </ListItem>
        )
    }
}

const mapStateToProps = (state: ApplicationState): StoreProps => {
    return {
        selectedConversation: state.chats.selectedConversation,
        username: state.auth.username
    }
}
const mapDispatchToProps: Actions = {
    selectConversationAndGetMessages,
}

export default connect(mapStateToProps, mapDispatchToProps)(
    withStyles(styles)(ConversationListItem)
)