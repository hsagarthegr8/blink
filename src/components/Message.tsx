import React, { FC } from 'react'
import { Message } from '../store/chats';
import { Typography, createStyles, Theme, withStyles, WithStyles, Card, CardContent, Tooltip } from '@material-ui/core';
import { CheckAll } from 'mdi-material-ui'
import { ApplicationState } from '../store';
import { connect } from 'react-redux';
import { parseTime } from '../utils';

interface OwnProps {
    message: Message
}

interface StoreProps {
    username?: string
}

const styles = (theme: Theme) => createStyles({
    sent: {
        float: 'right'
    },

    received: {
        float: 'left'
    },

    message: {
        margin: '1px 1px 4px 1px',
        display: 'block',
        clear: 'both',
        minWidth: '5em',
        maxWidth: '40em'
    },

    messageContent: {
        padding: "4px 0px 0px 6px",
        display: 'inline'
    },

    time: {
        float: 'right',
        display: 'block',
        padding: '0px 4px 2px 2px',
        marginTop: '1em',
        marginLeft: '2em'
    },

    read: {
        float: 'right',
        display: 'block',
        padding: '6px 5px 0px 0px'
    }
})

const MessageComponent: FC<OwnProps & StoreProps & WithStyles<typeof styles>> = (props) => {
    const { message, username, classes } = props
    const messageRead = message.read && message.sender === username
    return (
        <div className={`${classes.message} ${message.sender === username ? classes.sent : classes.received}`}>
            <Card className={`${classes.message} ${message.sender === username ? classes.sent : classes.received}`}>
                <CardContent className={classes.messageContent}>
                    <Typography display="inline" variant="body1">
                        {message.content}
                    </Typography>
                </CardContent>
                {messageRead &&

                    <Typography variant="caption"
                        display="inline"
                        className={classes.read}
                    >
                        <Tooltip title={`Read ${parseTime(message.updated)}`} >
                            <CheckAll color='primary' fontSize="small" />
                        </Tooltip>
                    </Typography>
                }
                <Typography
                    display="inline"
                    variant="caption"
                    className={classes.time}
                >
                    {parseTime(message.timestamp)}
                </Typography>

            </Card>
        </div>
    )
}

const mapStateToProps = (state: ApplicationState): StoreProps => ({
    username: state.auth.username
})

export default connect(mapStateToProps)(withStyles(styles)(MessageComponent))