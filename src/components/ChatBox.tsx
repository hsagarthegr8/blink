import React, { Component, Fragment } from 'react'
import { ApplicationState } from '../store'
import { connect } from 'react-redux'
import { Grid } from '@material-ui/core';
import ConversationBox from './ConversationsBox';
import { sendAuthorization } from '../store/websocket/action';
import MessageBox from './MessageBox';


interface StoreProps {
    token?: string
}

interface Action {
    sendAuthorization: Function
}



class ChatBox extends Component<StoreProps & Action> {
    componentDidMount() {
        const { sendAuthorization, token } = this.props
        if (token) {
            sendAuthorization(token)
        }

    }

    render() {
        return (
            <Fragment>
                <Grid item  style={{ height: '100%' }}>
                    <ConversationBox />
                </Grid>
                <Grid item sm container>
                    <Grid item xs style={{ height: '100%' }}>
                        <MessageBox />
                    </Grid>
                </Grid>
            </Fragment>
        )
    }
}

const mapStateToProps = (state: ApplicationState): StoreProps => {
    return {
        token: state.auth.token
    }
}

const mapDispatchToProps: Action = {
    sendAuthorization
}

export default connect(mapStateToProps, mapDispatchToProps)(ChatBox)