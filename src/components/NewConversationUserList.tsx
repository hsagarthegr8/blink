import React, { Component } from 'react'
import { User } from '../store/user';
import { ApplicationState } from '../store';
import { connect } from 'react-redux';
import {
    ExpansionPanel, ExpansionPanelSummary, Typography, ExpansionPanelDetails,
    TextField, Button, Avatar, Theme, createStyles, withStyles, WithStyles
} from '@material-ui/core';
import avatar from '../images/user.png'
import { selectConversationAndGetMessages } from '../store/chats';
import { sendMessageRequest } from '../store/websocket/action';

interface OwnProps {
    handleClose: Function
}
interface StoreProps {
    filteredUsers: Array<User>,
    selectedConversation: string
    username?: string
}

interface Action {
    sendMessageRequest: Function,
    selectConversationAndGetMessages: Function
}


interface State {
    expanded?: string,
    message: string
}

const styles = (theme: Theme) => createStyles({
    avatar: {
        height: '5%',
        width: '5%'
    },

    userName: {
        marginLeft: '0.5em',
        fontSize: 'large'
    },

    userArea: {
        marginTop: '2px'
    },

    messageArea: {
        padding: '0 0.5em 1em 1em'
    },

    button: {
        marginLeft: '1em'
    }
})

class NewConversationUserList extends Component<OwnProps & StoreProps & Action & WithStyles<typeof styles>, State> {
    state = {
        expanded: 'unknown',
        message: ''
    }

    handleExpandPanel = (username: string) => {
        this.setState(prevState => ({
            ...prevState,
            expanded: prevState.expanded === username ? 'unknown' : username,
            message: ''
        }))
    }

    handleMessageChange = (value: string) => {
        this.setState({
            message: value
        })
    }

    handleMessageSend = (recipent: string) => {
        const { username, handleClose,
            selectConversationAndGetMessages, sendMessageRequest } = this.props
        if (username) {
            const users = [username, recipent].sort()
            const conversationId = users.join('<->')
            sendMessageRequest(conversationId, this.state.message)
            this.handleMessageChange('')
            handleClose()
            selectConversationAndGetMessages(conversationId)
        }

    }

    render() {
        const { filteredUsers, classes } = this.props
        const { expanded, message } = this.state
        return (
            filteredUsers.length ? (
                filteredUsers.map((user: User) => (
                    <ExpansionPanel key={user.username}
                        className={classes.userArea}
                        expanded={expanded === user.username}
                        onChange={() => this.handleExpandPanel(user.username)}
                    >
                        <ExpansionPanelSummary>
                            <Avatar className={classes.avatar} alt={user.fullName} src={
                                user.image_url || avatar
                            } />
                            <Typography className={classes.userName}>
                                {user.fullName}
                            </Typography>
                        </ExpansionPanelSummary>

                        <ExpansionPanelDetails className={classes.messageArea}>
                            <TextField
                                placeholder="Enter your Message"
                                fullWidth
                                value={message}
                                onChange={(e) => this.handleMessageChange(e.target.value)}
                            />

                            <Button
                                variant="contained"
                                color="primary"
                                className={classes.button}
                                disabled={!Boolean(message)}
                                onClick={() => this.handleMessageSend(user.username)}
                            >
                                <Typography color="inherit">Send</Typography>
                            </Button>

                        </ExpansionPanelDetails>

                    </ExpansionPanel>
                ))
            )
                :
                null
        )
    }
}

const mapStateToProps = (state: ApplicationState): StoreProps => {
    return {
        filteredUsers: state.users.filteredUsers,
        selectedConversation: state.chats.selectedConversation,
        username: state.auth.username
    }
}

const mapDispatchToProps: Action = {
    sendMessageRequest,
    selectConversationAndGetMessages
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(NewConversationUserList))