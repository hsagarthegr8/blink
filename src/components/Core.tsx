import React, { FC } from 'react'
import { Provider } from 'react-redux'
import { BrowserRouter } from 'react-router-dom'
import { MuiPickersUtilsProvider } from '@material-ui/pickers'
import MomentUtils from '@date-io/moment'

import store from '../store'
import App from './App'

const core: FC = () => 
    <Provider store={store}>
        <BrowserRouter>
            <MuiPickersUtilsProvider utils={MomentUtils}>
                <App />
            </MuiPickersUtilsProvider>
        </BrowserRouter>
    </Provider>

export default core