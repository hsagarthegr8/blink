import React, { FC } from 'react'
import { Message, Conversations } from '../store/chats';
import { ApplicationState } from '../store';
import { connect } from 'react-redux';
import { Scrollbar } from 'react-scrollbars-custom'
import MessageComponent from './Message';


interface StoreProps {
    conversations: Conversations
    selectedConversation: string
}

const MessageArea: FC<StoreProps> = props => {
    const { selectedConversation, conversations } = props
    const id = selectedConversation
    const messages = id ? conversations[id] : undefined
    

    return (
        <Scrollbar style={{height:'90%'}} scrollTop={1000}>
            <div>
                {selectedConversation ? ( 
                    messages ? (
                        messages.length ?
                        messages.map((message: Message) => {
                            return (
                                <MessageComponent key={message.id} message={message} />
                            )
                        })
                        :
                        null
                    )
                    
                    : 
                    null
                    )
                :
                    null
                }
            </div>
        </Scrollbar>
        
    )
}

const mapStateToProps = (state: ApplicationState): StoreProps => {
    return {
        conversations: state.chats.conversations,
        selectedConversation: state.chats.selectedConversation
    }
}

export default connect(mapStateToProps)(MessageArea)