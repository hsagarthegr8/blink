import React, { Component, Fragment } from 'react'
import { Paper } from '@material-ui/core';
import MessageArea from './MessagesArea';
import MessageForm from './forms/MessageForm';
import { ApplicationState } from '../store';
import { connect } from 'react-redux';


interface StoreProps {
    selectedConversation: string
}

class MessageBox extends Component<StoreProps> {
    render() {
        const { selectedConversation } = this.props
        return (
            <Paper square style={{height: '100%', padding: '1em'}}>
                { selectedConversation &&
                    <Fragment>
                        <MessageArea />
                        <MessageForm />
                    </Fragment>
                }
                
            </Paper>

        )
    }
}

const mapStateToProps = (state: ApplicationState): StoreProps => ({
    selectedConversation: state.chats.selectedConversation
})

export default connect(mapStateToProps)(MessageBox)