import React, { Component, Fragment } from 'react'
import { CssBaseline } from '@material-ui/core'

import { NavBar, MainContent } from './layout'

import background from '../images/trianglify.svg';

class App extends Component {
  render() {
    return (
      <Fragment>
        <CssBaseline />
        <div style={{backgroundImage: `url(${background})`, backgroundSize:'cover', height:'100%'}}>
          <NavBar />
          <MainContent />
        </div>
      </Fragment>
    )
  }
}

export default App
