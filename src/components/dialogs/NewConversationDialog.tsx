import React, { Component, Fragment } from 'react'
import { Theme, createStyles, withStyles, WithStyles, 
    Fab, Dialog, DialogTitle, DialogContent 
} from '@material-ui/core';
import { Plus } from 'mdi-material-ui'
import { debounce } from 'lodash'
import Scrollbar from 'react-scrollbars-custom';
import { searchUserWebsocket } from '../../store/websocket/action';
import { connect } from 'react-redux';
import { clearUserSearch } from '../../store/user';
import { UserSearchForm } from '../forms';
import NewConversationUserList from '../NewConversationUserList';

const styles = (theme: Theme) => createStyles({
    fab: {
        position: 'absolute',
        bottom: '1em',
        right: '1em'
    },
    
    dialogContent: {
        paddingTop: 2
    },


})

interface Action {
    searchUser: Function,
    clearUserSearch: Function
}

interface State {
    open: boolean,
    query: string
}

class NewConversationDialog extends Component<Action & WithStyles<typeof styles>, State> {
    state = {
        open: false,
        query: ''
    }

    handleDialogOpen = () => {
        this.setState({
            open: true
        })
    }
    
    handleDialogClose = () => {
        const { clearUserSearch } = this.props
        clearUserSearch()
        this.setState({
            open: false,
            query: ''
        })
    }

    handleChange = (value: string) => {
        this.setState({
            query: value
        })
        this.handleSearch()
    }

    handleSearch = debounce(()=>{
        const { searchUser } = this.props
        searchUser(this.state.query)
    }, 500)


    render() {
        const { classes } = this.props
        const { open, query } = this.state
        return (
            <Fragment>
                <div className={classes.fab}>
                    <Fab color="primary" onClick={this.handleDialogOpen}>
                        <Plus />
                    </Fab>
                </div>
                <Dialog 
                    open={open}
                    onClose={this.handleDialogClose}
                    fullWidth
                >
                    <DialogTitle>New Conversation</DialogTitle>
                    <DialogContent className={classes.dialogContent}>
                        <UserSearchForm query={query} handleChange={this.handleChange} />
                        <Scrollbar style={{height:'16em', marginTop:'1em'}}>
                            <NewConversationUserList handleClose={this.handleDialogClose}/>
                        </Scrollbar>
                    </DialogContent>
                </Dialog>
            </Fragment>
        )
    }
}

const mapDispatchToProps: Action = {
    searchUser: searchUserWebsocket,
    clearUserSearch: clearUserSearch
}

export default connect(null, mapDispatchToProps)(withStyles(styles)(NewConversationDialog))