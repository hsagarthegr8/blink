import React, { Component, Fragment } from 'react'
import { Link } from 'react-router-dom'
import {
    AppBar, Typography, Toolbar,
    Theme, createStyles, withStyles, WithStyles
} from '@material-ui/core'
import { withRouter, RouteComponentProps } from 'react-router-dom'
import { connect } from 'react-redux'
import { LoginVariant, LogoutVariant, AccountPlus, AccountCardDetailsOutline, MessageOutline } from 'mdi-material-ui'

import NavButton from '../buttons/NavButton'
import { AuthState, logOut } from '../../store/auth'
import { ApplicationState } from '../../store'
import BadgeNavButton from '../buttons/BadgeNavButton';



const styles = (theme: Theme) => createStyles({
    grow: {
        flexGrow: 1
    },
    nav: {
        backgroundColor: 'rgba(255,255,255,0)',
        boxShadow: 'none',
        height: '10%'
    },
    navLink: {
        textDecoration: 'none'
    }
})


interface StoreProps {
    auth: AuthState,
    unreads: number
}

interface Actions {
    logOut: Function
}

class NavBar extends Component<RouteComponentProps & StoreProps
    & Actions & WithStyles<typeof styles>> {

    handleLogOut = () => {
        const { logOut, history } = this.props
        logOut()
        history.push('/')
    }

    render() {
        const { classes, auth, unreads } = this.props
        return (
            <AppBar position="static" color="primary" className={classes.nav}>
                <Toolbar>
                    <Link to='/' className={classes.navLink}>
                        <Typography variant="h6" color="primary" >
                            Blink Messenger
                        </Typography>
                    </Link>

                    <div className={classes.grow}></div>
                    {!auth.isLoggedIn ?
                        <Fragment>
                            <Link to='/login/' className={classes.navLink}>
                                <NavButton
                                    title="Log In"
                                    Icon={LoginVariant}
                                    onClick={() => { }}
                                />
                            </Link>
                            <Link to='/register/' className={classes.navLink}>
                                <NavButton
                                    title="Register"
                                    Icon={AccountPlus}
                                    onClick={() => { }}
                                />
                            </Link>

                        </Fragment>
                        :
                        <Fragment>
                            <Link to='/chats/' className={classes.navLink}>
                                <BadgeNavButton
                                    badge={unreads}
                                    title="Chats"
                                    Icon={MessageOutline}
                                    onClick={() => { }}
                                />
                            </Link>

                            <Link to='/profile/' className={classes.navLink}>
                                <NavButton
                                    title="Profile"
                                    Icon={AccountCardDetailsOutline}
                                    onClick={() => { }}
                                />
                            </Link>
                            <NavButton
                                title="Log Out"
                                Icon={LogoutVariant}
                                onClick={this.handleLogOut}
                            />
                        </Fragment>

                    }
                </Toolbar>
            </AppBar>
        )
    }
}

const mapStateToProps = (state: ApplicationState): StoreProps => ({
    auth: state.auth,
    unreads: state.chats.unreadCounts
})

const mapDispatchToProps: Actions = {
    logOut
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter((withStyles(styles)(NavBar))))