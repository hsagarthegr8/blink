import NavBar from './Nav'
import MainContent from './MainContent'

export {
    NavBar,
    MainContent
}