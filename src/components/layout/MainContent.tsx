import React, { FC } from 'react'
import { Grid, createStyles, WithStyles, withStyles } from '@material-ui/core'
import { Route } from 'react-router';
import { RegisterForm, LoginForm } from '../forms';
import ChatBox from '../ChatBox';
import { ApplicationState } from '../../store';
import { connect } from 'react-redux';
import NewUserWizard from '../steppers/NewUserWizard/NewUserWizard';

const styles = () => createStyles({
    container: {
        height: '90%',
        paddingLeft: '1em',
        paddingRight: '1em',
        paddingBottom: '1em'
    }
})

interface StoreProps {
    isLoggedIn: boolean
}

const MainContent: FC<StoreProps & WithStyles<typeof styles>> = props => {
    const { classes, isLoggedIn } = props
    return (
        <Grid container className={classes.container} alignItems="center" >
            <Route path='/login/' component={LoginForm} />
            <Route path='/register/' component={NewUserWizard} />
            {isLoggedIn &&
                <Grid container style={{ height: '100%' }}>
                    <Route path='/chats/' component={ChatBox} />
                    <Route path='/welcome/' component={NewUserWizard} />
                </Grid>
            }
        </Grid>
    )
}

const mapStateToProps = (state: ApplicationState): StoreProps => {
    return {
        isLoggedIn: state.auth.isLoggedIn
    }
}

export default connect(mapStateToProps)(withStyles(styles)(MainContent))