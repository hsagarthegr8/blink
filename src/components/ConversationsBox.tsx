import React, { Component, ChangeEvent } from 'react'
import { Paper, Theme, createStyles, withStyles, WithStyles } from '@material-ui/core';
import ConversationList from './ConversationList';
import { ConversationSearchForm } from './forms';
import { NewConversationDialog } from './dialogs';

const styles = (theme: Theme) => createStyles({
    root: {
        height: '100%',
        padding:'2px',
        width: '100%',
        minWidth: '22em',
        position: 'relative'
    }
})


interface State {
    query: string
}

class ConversationBox extends Component<WithStyles<typeof styles>, State> {
    state={
        query: ''
    }

    handleQueryChange = (event: ChangeEvent<HTMLInputElement>, reset:boolean = false) =>{
        this.setState({
            query: reset ? '' : event.target.value
        })
    }

    render() {
        const { classes } = this.props
        const { query } = this.state
        return (
            <Paper square className={classes.root}>
                <ConversationSearchForm query={query} handleChange={this.handleQueryChange}/>
                <ConversationList query={query}/>             
                <NewConversationDialog />   
            </Paper>
        )
    }
}

export default withStyles(styles)(ConversationBox)