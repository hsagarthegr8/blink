import Welcome from './Step1-Welcome'
import NewProfileForm from './Step2-NewProfileForm'
import ProfilePictureForm from './Step3-ProfilePicture'
import ItsAllDone from './Step4-ItsAllDone';


export {
    Welcome,
    NewProfileForm,
    ProfilePictureForm,
    ItsAllDone
}