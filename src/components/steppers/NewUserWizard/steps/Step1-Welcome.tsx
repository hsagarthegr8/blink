import React, { FC } from 'react'
import { Grid, Typography } from '@material-ui/core'

const Welcome: FC = () => {
    return (
        <Grid item xs={6} style={{ margin: 'auto' }}>
            
            <Typography align="center" variant="h4" color="primary" >
                Welcome
            </Typography>
            <div style={{ margin: '2em' }}></div>
            <Typography align="center">
                We will guide you to setup your profile.
                For that we will need some basic details about you.
                Click Next Button whenever you are ready.
            </Typography>
        </Grid>
    )
}

export default Welcome