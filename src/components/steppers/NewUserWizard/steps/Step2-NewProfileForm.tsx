import React, { Component } from 'react'
import {
    createStyles, Theme, WithStyles, withStyles,
    Typography, TextField, Grid, RadioGroup, Radio, FormControlLabel, InputLabel
} from '@material-ui/core'
import { FormikProps } from 'formik'
import { KeyboardDatePicker } from '@material-ui/pickers';
import { MaterialUiPickersDate } from '@material-ui/pickers/typings/date';
import { RouteComponentProps } from 'react-router-dom'
import { Values } from '../NewUserWizard'
import { ErrorRadio } from '../../../buttons';



const styles = (theme: Theme) => createStyles({
    text: {
        padding: 8
    },
    buttonGroup: {
        float: "right",
        marginTop: '1em'
    },
    formButton: {
        marginLeft: '1em'
    },
    formControl: {
        marginTop: '1em'
    },

    formControlLabel: {
        paddingTop: '1em'
    }
})

class NewProfileForm extends Component<FormikProps<Values> & RouteComponentProps & WithStyles<typeof styles>> {
    handleChange = (event: any) => {
        const { handleChange, setFieldTouched } = this.props
        handleChange(event)
        setFieldTouched(event.target.name)
    }

    handleDateChange = (value: MaterialUiPickersDate) => {
        const {
            setFieldValue,
            setFieldTouched
        } = this.props
        setFieldValue("date_of_birth", value, true)
        setFieldTouched("date_of_birth")
    }

    render() {
        const { errors, touched, values, classes } = this.props
        return (
            <Grid item xs={6} style={{ margin: 'auto' }}>
                <Typography variant="h5">Enter Profile Details</Typography>
                <form>
                    <Grid container className={classes.formControl}>
                        <Grid item xs={4}>
                            <InputLabel required className={classes.formControlLabel}>First Name:</InputLabel>
                        </Grid>
                        <Grid item xs={8}>
                            <TextField
                                name="first_name"
                                value={values.first_name}
                                variant="outlined"

                                helperText={touched.first_name ? errors.first_name : ""}
                                error={(touched.first_name && Boolean(errors.first_name))}
                                required
                                fullWidth
                                onChange={this.handleChange}
                                inputProps={{
                                    className: classes.text
                                }}
                            />
                        </Grid>
                    </Grid>

                    <Grid container className={classes.formControl}>
                        <Grid item xs={4}>
                            <InputLabel className={classes.formControlLabel}>Last Name:</InputLabel>
                        </Grid>
                        <Grid item xs={8}>
                            <TextField
                                name="last_name"
                                value={values.last_name}
                                variant="outlined"
                                helperText={touched.last_name ? errors.last_name : ""}
                                error={(touched.last_name && Boolean(errors.last_name))}
                                required
                                fullWidth
                                onChange={this.handleChange}
                                inputProps={{
                                    className: classes.text
                                }}
                            />
                        </Grid>
                    </Grid>

                    <Grid container className={classes.formControl}>
                        <Grid item xs={4}>
                            <InputLabel required className={classes.formControlLabel}>Gender:</InputLabel>
                        </Grid>
                        <Grid item xs={8}>
                            <Grid item>
                                <RadioGroup
                                    name="gender"
                                    value={values.gender}
                                    onChange={this.handleChange}
                                    style={{ display: 'flex', flexDirection: 'row' }}
                                >
                                    <FormControlLabel
                                        value="F"
                                        label="Female"
                                        labelPlacement="end"
                                        control={(touched.gender && Boolean(errors.gender)) ?
                                            <ErrorRadio /> :
                                            <Radio color="primary" />
                                        }
                                    />
                                    <FormControlLabel
                                        value="M"
                                        label="Male"
                                        labelPlacement="end"
                                        control={(touched.gender && Boolean(errors.gender)) ?
                                            <ErrorRadio /> :
                                            <Radio color="primary" />
                                        }
                                    />
                                </RadioGroup>
                            </Grid>
                            <Grid item>
                                <Typography
                                    color="error"
                                    variant="caption"
                                    style={{ marginLeft: '1em' }}
                                >
                                    {touched.gender ? errors.gender : null}
                                </Typography>
                            </Grid>
                        </Grid>

                    </Grid>

                    <Grid container className={classes.formControl}>
                        <Grid item xs={4}>
                            <InputLabel required className={classes.formControlLabel}>Date of Birth:</InputLabel>
                        </Grid>
                        <Grid item xs={8}>
                            <KeyboardDatePicker
                                name="date_of_birth"
                                variant="dialog"
                                inputVariant="outlined"
                                value={values.date_of_birth}
                                onChange={this.handleDateChange}
                                format="DD/MM/YYYY"
                                maxDate={new Date()}
                            />
                        </Grid>
                    </Grid>

                    <Grid container className={classes.formControl}>
                        <Grid item xs={4}>
                            <InputLabel className={classes.formControlLabel}>About:</InputLabel>
                        </Grid>
                        <Grid item xs={8}>
                            <TextField
                                name="about"
                                variant="outlined"
                                value={values.about}
                                helperText={touched.about ? errors.about : ""}
                                error={(touched.about && Boolean(errors.about))}
                                required
                                fullWidth
                                multiline
                                rows={5}
                                onChange={this.handleChange}
                            />
                        </Grid>
                    </Grid>



                    {/* <div className={classes.buttonGroup}>
                        <Button
                            className={classes.formButton}
                            variant="contained"
                            color="primary"
                            onClick={() => handleSubmit()}
                        >
                            Save
                        </Button>

                        <Button
                            className={classes.formButton}
                            variant="contained"
                            color="primary"
                            onClick={() => resetForm()}
                        >
                            Reset
                        </Button>
                    </div> */}
                </form>
            </Grid>
        )
    }
}

export default withStyles(styles)(NewProfileForm)