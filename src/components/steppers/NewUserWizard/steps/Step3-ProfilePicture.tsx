import React, { FC } from 'react'
import { Uploader } from '../../../widgets'
import { Grid, Avatar, InputLabel, Typography } from '@material-ui/core';
import { FormikProps } from 'formik';
import { Values } from '../NewUserWizard'
import avatar from '../../../../images/user.png'

const ProfilePictureForm: FC<FormikProps<Values>> = props => {
    const { values, setFieldValue } = props
    return (
        <Grid container item alignContent="center" justify="center">
            <InputLabel style={{ display: 'inline', margin: '1em', position: 'relative', bottom: '6px' }}>Select Profile Picture:</InputLabel>
            <Uploader
                id='file'
                name='file'
                data-crop="1:1"
                onChange={(file: any) => {
                    console.log('File changed: ', file)

                    if (file) {
                        file.progress((info: any) => console.log('File progress: ', info.progress))
                        file.done((info: any) => console.log('File uploaded: ', info))
                    }
                }}
                onUploadComplete={(info: any) => {
                    console.log('Upload completed:', info)
                    setFieldValue('image_url', info.cdnUrl, true)
                }
                }
            />
            <Grid container item alignContent="center" justify="center">
                <Avatar src={values.image_url || avatar} style={{ height: '10em', width: '10em', margin: 'auto' }} />
            </Grid>
        </Grid>
    )
}

export default ProfilePictureForm