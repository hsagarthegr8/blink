import React, { Component } from 'react'
import { Stepper, Step, StepLabel, Paper, Button, Grid } from '@material-ui/core'
import { withFormik, FormikBag, FormikProps } from 'formik'
import { withRouter, RouteComponentProps } from 'react-router-dom'
import { AxiosResponse, AxiosError } from 'axios'
import * as Yup from 'yup'
import moment from 'moment'

import { Welcome, NewProfileForm, ProfilePictureForm, ItsAllDone } from './steps'
import Api, { newProfileUrl } from '../../../api'

export interface Values {
    first_name: string,
    last_name?: string,
    gender: string,
    date_of_birth: Date,
    about?: string,
    image_url?: string
}



const formContainer = withFormik<any, Values>({
    mapPropsToValues: (): Values => ({
        first_name: '',
        last_name: '',
        gender: '',
        date_of_birth: new Date(),
        about: '',
        image_url: '',
    }),

    validationSchema: Yup.object().shape<Values>({
        first_name: Yup.string().required(),
        last_name: Yup.string(),
        gender: Yup.string().required(),
        date_of_birth: Yup.date(),
        about: Yup.string(),
        image_url: Yup.string()
    }),

    handleSubmit: (values: Values, formikBag: FormikBag<RouteComponentProps, Values>) => {
        const api = Api()
        api.post(newProfileUrl, {
            ...values,
            date_of_birth: moment(values.date_of_birth).format('YYYY-MM-DD')
        })
            .then((res: AxiosResponse) => {
                formikBag.props.history.push('/chats/')
            })
            .catch((err: AxiosError) => {
                console.log(err)
            })
    }
})


interface State {
    activeStep: number
}


class NewUserWizard extends Component<RouteComponentProps & FormikProps<Values>, State> {

    state = {
        activeStep: 0
    }

    getStepLabel = (stepIndex: number) => {
        switch (stepIndex) {
            case 0:
                return 'Greetings...'

            case 1:
                return 'Enter Basic Profile Details'

            case 2:
                return 'Set Profile Picture'

            case 3:
                return 'Its all done'

            default:
                return 'Invalid Step'
        }
    }

    getStepContent = (stepIndex: number) => {
        switch (stepIndex) {
            case 0:
                return <Welcome />
            case 1:
                return <NewProfileForm {...this.props} />
            case 2:
                return <ProfilePictureForm {...this.props} />
            case 3:
                return <ItsAllDone />
            default:
                return null
        }
    }

    handleFormNext = () => {
        const { setTouched, isValid } = this.props
        setTouched({
            first_name: true,
            gender: true
        })
        if (isValid) {
            this.setState(prevState => ({
                activeStep: prevState.activeStep + 1
            }))
        }

    }

    handleNext = () => {
        if (this.state.activeStep === 1) {
            this.handleFormNext()
        }
        else {
            this.setState(prevState => ({
                activeStep: prevState.activeStep + 1
            }))
        }
    }

    render() {
        const { handleSubmit } = this.props
        const steps = [0, 1, 2, 3]
        const { activeStep } = this.state
        return (
            <Paper style={{ width: '100%', height: '100%', padding: '1em' }}>
                <Stepper activeStep={activeStep}>
                    {steps.map((label, index) => (
                        <Step key={label}>
                            <StepLabel>{this.getStepLabel(index)}</StepLabel>
                        </Step>
                    ))}
                </Stepper>
                <div style={{ height: '75%' }}>
                    <Grid container justify="center" style={{ height: '100%' }}>
                        {this.getStepContent(activeStep)}
                    </Grid>
                    <div style={{ float: "right", padding: '1em' }}>
                        <Button
                            variant="contained"
                            color="primary"
                            onClick={activeStep === steps.length - 1 ?
                                () => handleSubmit() :
                                this.handleNext
                            }
                        >
                            {activeStep === steps.length - 1 ? 'Finish' : 'Next'}
                        </Button>
                    </div>
                </div>
            </Paper >
        )
    }
}


export default withRouter(formContainer(NewUserWizard))