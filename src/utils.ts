import moment from 'moment'

export const requiredErrorMessage = 'This field is required'


export const parseTime = (time: Date) => {
    let parsedTime = moment(time).format('DD/MM/YYYY')
    if (parsedTime === moment().format('DD/MM/YYYY')) {
        if (moment().diff(time, 'seconds') < 59) {
            return 'Just Now'
        }
        else {
            return moment(time).format('h:mm a')
        }
    }
    return moment(time).format('DD/MM/YYYY')
}