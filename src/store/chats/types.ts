import { User } from "../user";

export interface ConversationBasic {
    id: string,
    recipient: User,
    message: Message,
    unread: boolean,
    timestamp: Date,
    updated: Date
}

export interface Message {
    id: number,
    content: string,
    timestamp: Date,
    updated: Date,
    sent: boolean,
    received: boolean,
    read: boolean,
    deleted_by_sender: boolean,
    deleted_by_receiver: boolean,
    sender: string
}

export interface ConversationList extends Array<ConversationBasic> { }

export interface Conversations {
    [id: string]: Array<Message>
}

export interface ChatsState {
    conversationList: ConversationList,
    selectedConversation: string,
    conversations: Conversations,
    unreadCounts: number
}

export const CONVERSATION_LIST = 'CONVERSATION_LIST'
export const SELECT_CONVERSATION = 'SELECT_CONVERSATION'
export const GET_CONVERSATION = 'GET_CONVERSATION'
export const CONVERSATION = 'CONVERSATION'
export const SEND_MESSAGE = 'SEND_MESSAGE'
export const RECEIVE_MESSAGE = 'RECEIVE_MESSAGE'

export interface GetConversationListAction {
    type: typeof CONVERSATION_LIST,
    conversations: ConversationList
}

export interface SelectConversationAction {
    type: typeof SELECT_CONVERSATION,
    conversation: string
}

export interface GetConversationRequestAction {
    type: typeof GET_CONVERSATION
    conversation_id: string
}

export interface GetConversationAction {
    type: typeof CONVERSATION,
    conversation: {
        id: string,
        messages: Array<Message>
    }
}

export interface SendMessageAction {
    type: typeof SEND_MESSAGE,
    conversation: string,
    content: string
}


export interface ReceiveMessageAction {
    type: typeof RECEIVE_MESSAGE,
    conversation: string,
    message: Message
}


export type ChatsActionTypes = GetConversationListAction | SelectConversationAction
    | GetConversationRequestAction | GetConversationAction | SendMessageAction | ReceiveMessageAction