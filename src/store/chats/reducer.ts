import { ChatsState, ChatsActionTypes, 
    SELECT_CONVERSATION, CONVERSATION_LIST, CONVERSATION, RECEIVE_MESSAGE } from "./types"

const initialState: ChatsState = {
    conversationList: [],
    selectedConversation: '',
    conversations: {},
    unreadCounts: 0
}


export const chatsReducer = (state=initialState, action: ChatsActionTypes): ChatsState => {
    switch(action.type) {
        case CONVERSATION_LIST:
            return {
                ...state,
                conversationList: action.conversations,
                unreadCounts: action.conversations.reduce((total, conversation) => 
                total + Number(conversation.unread), 0)
            }
        case SELECT_CONVERSATION:
            return {
                ...state,
                selectedConversation: action.conversation
            }

        case CONVERSATION:
            return {
                ...state,
                conversations: {
                    ...state.conversations,
                    [action.conversation.id]: action.conversation.messages
                }
            }
        
        case RECEIVE_MESSAGE:
            return {
                ...state,       
                conversations: {
                    ...state.conversations,
                    [action.conversation]: state.conversations[action.conversation] ? 
                    [...state.conversations[action.conversation], action.message] : [action.message]
                }        
            }
            
        default:
            return state
    }
}

