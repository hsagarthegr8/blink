import { ConversationList, CONVERSATION_LIST, SELECT_CONVERSATION, 
    GetConversationListAction, SelectConversationAction, GET_CONVERSATION, 
    GetConversationRequestAction, SendMessageAction, SEND_MESSAGE
} from "./types"
import { getConversationWebsocketRequest } from "../websocket/action";
import { ThunkDispatch } from "redux-thunk";

export const getConversations = (conversations: ConversationList): GetConversationListAction => {
    return {
        type: CONVERSATION_LIST,
        conversations: conversations
    }
}

export const selectConversation = (conversation: string): SelectConversationAction => {
    return {
        type: SELECT_CONVERSATION,
        conversation: conversation
    }
}

export const getConversationRequest = (conversation: string): GetConversationRequestAction => {
    return {
        type: GET_CONVERSATION,
        conversation_id: conversation
    }
}


export const sendMessage = (conversation: string, message: string): SendMessageAction => {
    return {
        type: SEND_MESSAGE,
        conversation: conversation,
        content: message
    }
}



export const selectConversationAndGetMessages = (conversation: string) => (dispatch: ThunkDispatch<any, any, any>) => {
    dispatch(selectConversation(conversation))
    dispatch(getConversationWebsocketRequest(conversation))
}

