import { createStore, combineReducers, applyMiddleware} from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import thunk from 'redux-thunk'

import { authReducer } from './auth'
import { chatsReducer } from './chats'
import reduxWebsocket from './websocket-middleware';
import userReducer from './user/reducer';


const reducers = combineReducers({
    auth: authReducer,
    chats: chatsReducer,
    users: userReducer
})

const store = createStore(reducers, composeWithDevTools(applyMiddleware(thunk, reduxWebsocket)))

export default store

export * from './types'