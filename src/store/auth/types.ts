export interface AuthState {
    isLoggedIn: boolean,
    username?: string,
    token?: string,
    tokenExpires?: Date
}


export const LOGIN = 'LOGIN'
export const LOGOUT = 'LOGOUT'
export const AUTHORIZATION = 'AUTHORIZATION'


export interface LogInAction {
    type: typeof LOGIN,
    payload: AuthState
}

export interface LogOutAction {
    type: typeof LOGOUT,
}

export interface AuthorizationAction {
    type: typeof AUTHORIZATION,
    token: string
}


export type AuthActionTypes = LogInAction | LogOutAction