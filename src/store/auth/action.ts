import { LOGIN, LOGOUT, AuthorizationAction, AUTHORIZATION } from "./types";
import { Dispatch } from "redux";
import { WebSocketConnect, WebSocketDisconnect } from "../websocket-middleware/action";
import { WEBSOCKET_URL } from '../../api'

export const logIn = (token: string) => {
    return (dispatch: Dispatch) => {
        dispatch(WebSocketConnect(WEBSOCKET_URL))
        const tokenParts = token.split(/\./)
        const tokenDecoded = JSON.parse(window.atob(tokenParts[1]))
        const tokenExpires = new Date(tokenDecoded.exp * 1000)
        const username = tokenDecoded.username
        dispatch({
            type: LOGIN,
            payload: {
                isLoggedIn: true,
                username: username,
                token: token,
                tokenExpires: tokenExpires
            }
        })
    }
}

export const logOut = () => (dispatch: Dispatch) => {
    dispatch({type: LOGOUT})
    dispatch(WebSocketDisconnect())
}

export const authorization = (token: string): AuthorizationAction => {
    return {
        type: AUTHORIZATION,
        token: token
    }
}