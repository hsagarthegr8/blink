import { Dispatch, MiddlewareAPI, Middleware } from "redux";
import ReduxWebsocket from "./websocket";
import { WebSocketActionTypes, WEBSOCKET_CONNECT, WEBSOCKET_DISCONNECT, WEBSOCKET_SEND, WEBSOCKET_MESSAGE } from "./types";

const createMiddleware = ():Middleware => {
    const reduxWebSocket = new ReduxWebsocket()
    return (store: MiddlewareAPI) => (next: Dispatch) => (action: WebSocketActionTypes) => {
        switch (action.type) {
            case WEBSOCKET_CONNECT:
                reduxWebSocket.connect(store, action)
                break
            
            case WEBSOCKET_DISCONNECT:
                reduxWebSocket.disconnect()
                break

            case WEBSOCKET_SEND:
                reduxWebSocket.send(action)
                store.dispatch(action.payload)
                break
            
            case WEBSOCKET_MESSAGE:
                store.dispatch(action.payload)
        }
        return next(action)
    }
}


export default createMiddleware()