import { Dispatch } from 'redux'
import ReconnectingWebSocket, { Event, CloseEvent } from 'reconnecting-websocket';
import { WebSocketOpen, WebSocketClose, WebSocketMessage } from './action';

const createWebSocket = (dispatch: Dispatch, url: string) => {
    const ws = new ReconnectingWebSocket(url)

    ws.onopen = (event: Event) => {
        dispatch(WebSocketOpen(event))
    }
    ws.onclose = (event: CloseEvent) => {
        dispatch(WebSocketClose(event))
    }
    ws.onmessage = (event: MessageEvent) => {
        dispatch(WebSocketMessage(event))
    }
    return ws
}

export default createWebSocket