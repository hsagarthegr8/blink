import { MiddlewareAPI } from "redux";
import { WebSocketConnectAction, WebSocketSendAction } from "./types"
import createWebSocket from "./create-websocket"
import ReconnectingWebSocket from "reconnecting-websocket"



class ReduxWebsocket {
    websocket?: ReconnectingWebSocket
    
    connect({dispatch}: MiddlewareAPI, action: WebSocketConnectAction ) {
        if (this.websocket) {
            this.websocket.close()
        }
        this.websocket = createWebSocket(dispatch, action.url)
    }

    disconnect() {
        if (this.websocket) {
            this.websocket.close()
        }
        else {
            throw new Error('Socket connection not initialized')
        }
    }

    send({ payload }: WebSocketSendAction) {
        if (this.websocket) {
            this.websocket.send(JSON.stringify(payload))
        }
    }
}

export default ReduxWebsocket