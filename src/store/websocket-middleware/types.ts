import { Event } from "reconnecting-websocket/dist/events";
import { AnyAction } from "redux";

export const WEBSOCKET_CONNECT = 'WEBSOCKET_CONNECT'
export const WEBSOCKET_DISCONNECT = 'WEBSOCKET_DISCONNECT'
export const WEBSOCKET_SEND = 'WEBSOCKET_SEND'

export const WEBSOCKET_OPEN = 'WEBSOCKET_OPEN'
export const WEBSOCKET_CLOSE = 'WEBSOCKET_CLOSE'
export const WEBSOCKET_MESSAGE = 'WEBSOCKET_MESSAGE'

export interface WebSocketConnectAction {
    type: typeof WEBSOCKET_CONNECT,
    url: string
}

export interface WebSocketDisconnectAction {
    type: typeof WEBSOCKET_DISCONNECT
}

export interface WebSocketSendAction {
    type: typeof WEBSOCKET_SEND,
    payload: AnyAction
}

export interface WebSocketMessageAction {
    type: typeof WEBSOCKET_MESSAGE,
    event: MessageEvent,
    timestamp: Date,
    payload: AnyAction
}

export interface WebSocketOpenAction {
    type: typeof WEBSOCKET_OPEN,
    payload: {
        event: Event,
        timestamp: Date
    }
}

export interface WebSocketCloseAction {
    type: typeof WEBSOCKET_CLOSE,
    payload: {
        event: Event
        timestamp: Date
    }
}

export type WebSocketActionTypes = WebSocketConnectAction | WebSocketDisconnectAction | WebSocketSendAction | WebSocketMessageAction