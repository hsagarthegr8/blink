import { WebSocketConnectAction, WEBSOCKET_CONNECT, WebSocketDisconnectAction, WEBSOCKET_DISCONNECT, WebSocketOpenAction, WEBSOCKET_OPEN, WebSocketCloseAction, WEBSOCKET_CLOSE, WebSocketSendAction, WEBSOCKET_SEND, WebSocketMessageAction, WEBSOCKET_MESSAGE } from "./types";
import { Event } from "reconnecting-websocket";
import { AnyAction } from "redux";

export const WebSocketConnect = (url: string): WebSocketConnectAction => ({
    type: WEBSOCKET_CONNECT,
    url: url
})

export const WebSocketDisconnect = (): WebSocketDisconnectAction => ({
    type: WEBSOCKET_DISCONNECT
})

export const WebSocketSend = (payload: AnyAction): WebSocketSendAction => ({
    type: WEBSOCKET_SEND,
    payload: payload
})

export const WebSocketOpen = (event: Event): WebSocketOpenAction => ({
    type: WEBSOCKET_OPEN,
    payload: {
        event: event,
        timestamp: new Date()
    }
})

export const WebSocketClose = (event: Event): WebSocketCloseAction => ({
    type: WEBSOCKET_CLOSE,
    payload: {
        event: event,
        timestamp: new Date()
    }
})

export const WebSocketMessage = (message: MessageEvent): WebSocketMessageAction => {
    const messageAction = JSON.parse(message.data)
    return {
        type: WEBSOCKET_MESSAGE,
        event: message,
        timestamp: new Date(),
        payload: messageAction
    }
}