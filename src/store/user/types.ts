export interface User {
    username: string,
    fullName: string,
    gender: string
    image_url: string,
}

export interface UsersState {
    filteredUsers: Array<User>
}

export const SEARCH_USER = 'SEARCH_USER'
export const USER_SEARCH_RESULT = 'USER_SEARCH_RESULT'
export const CLEAR_USER_SEARCH = 'CLEAR_USER_SEARCH'

export interface SearchUserAction {
    type: typeof SEARCH_USER,
    query: string
}

export interface UserSearchResultAction {
    type: typeof USER_SEARCH_RESULT,
    users: Array<User>
}

export interface ClearUserSearchAction {
    type: typeof CLEAR_USER_SEARCH,
}

export type UserActionTypes = UserSearchResultAction | ClearUserSearchAction