import { UsersState, UserActionTypes, USER_SEARCH_RESULT, CLEAR_USER_SEARCH } from "./types";

const initialState: UsersState = {
    filteredUsers: []
}

const userReducer = (state=initialState, action: UserActionTypes): UsersState => {
    switch (action.type) {
        case USER_SEARCH_RESULT:
            return {
                ...state,
                filteredUsers: action.users
            }

        case CLEAR_USER_SEARCH:
            return {
                ...state,
                filteredUsers: []
            }
        
        default:
            return state
    }
}

export default userReducer