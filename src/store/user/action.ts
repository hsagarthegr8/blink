import { SearchUserAction, SEARCH_USER, CLEAR_USER_SEARCH, ClearUserSearchAction } from './types'

export const searchUser = (query: string): SearchUserAction => ({
    type: SEARCH_USER,
    query: query
})


export const clearUserSearch = (): ClearUserSearchAction => ({
    type: CLEAR_USER_SEARCH
})