import { AuthState } from './auth'
import { ChatsState } from './chats';
import { UsersState } from './user';

export interface ApplicationState {
    auth: AuthState,
    chats: ChatsState,
    users: UsersState
}

export type GetState = ()=> ApplicationState