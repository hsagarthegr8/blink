import { Dispatch } from 'redux';
import { WebSocketSend } from '../websocket-middleware/action';
import { authorization } from '../auth';
import { getConversationRequest, sendMessage } from '../chats';
import { searchUser } from '../user';


export const sendAuthorization = (token: string) => (dispatch: Dispatch) => {
    dispatch(WebSocketSend(authorization(token)))
}

export const sendMessageRequest = (conversation: string, message: string) => (dispatch: Dispatch) => {
    dispatch(WebSocketSend(sendMessage(conversation, message)))
}

export const getConversationWebsocketRequest = (conversation: string) => (dispatch: Dispatch) => {
    dispatch(WebSocketSend(getConversationRequest(conversation)))
}


export const searchUserWebsocket = (query: string) => (dispatch: Dispatch) => {
    dispatch(WebSocketSend(searchUser(query)))
}