import axios from 'axios'
import store from './store'

const LOCAL = true

const LOCAL_URL = 'http://127.0.0.1:8000/api/'
const REMOTE_URL = 'https://fringuante-chocolatine-15872.herokuapp.com/api/'
const BASE_URL = LOCAL ? LOCAL_URL : REMOTE_URL

const LOCAL_WEBSOCKET_URL = 'ws://127.0.0.1:8000/chats/'
const REMOTE_WEBSOCKET_URL = 'wss://fringuante-chocolatine-15872.herokuapp.com/chats/'
export const WEBSOCKET_URL = LOCAL ? LOCAL_WEBSOCKET_URL : REMOTE_WEBSOCKET_URL

const Api = () => {
    const state = store.getState().auth
    if (state.isLoggedIn) {
        return axios.create({
            baseURL: BASE_URL,
            headers: {
                Authorization: `JWT ${state.token}`
            }
        })
    }
    return axios.create({
        baseURL: BASE_URL
    })
}

export default Api

export const loginUrl = 'get-token/'
export const registerUrl = 'accounts/new/'
export const newProfileUrl = 'profiles/new/'